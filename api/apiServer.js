var express = require('express');

//files to server API
var members = require('./members.json');
var messages = require('./messages.json');

var app = express();
var port = 3001;
var cors = require('cors');

app.use(express.static('public'));
app.use(cors());

app.get('/api/messages', function (req, res) {
	var response = { response: messages.sort(compare), status: 'OK', statusCode: 200 };
	return res.json(response);
});

app.get('/api/members', function (req, res) {
	var response = { response: members, status: 'OK', statusCode: 200 };
	return res.json(response);
});

function compare(a, b) {
	a = new Date(a.timestamp);
	b = new Date(b.timestamp);
	if (a < b) return -1;
	else if (b > a) return 1;
	else return 0;
}

app.listen(3001);
console.log('Server listening on http://localhost:' + port);
