import { timeSince } from './time-utils';

export function serializeMessage(message, members) {
	const membersObj = members.reduce((accu, item) => {
		accu[item.id] = item;
		return accu;
	}, {});

	const serializedMessages = message.map(message => {
		const { firstName, lastName, email, avatar, userId } = membersObj[message.userId];
		return {
			...message,
			email,
			avatar,
			userId,
			timestamp: timeSince(message.timestamp),
			userName: `${firstName} ${lastName}`,
		};
	});

	return serializedMessages;
}
