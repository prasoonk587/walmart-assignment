export function get(url) {
	return fetch(url, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
		},
	})
		.then(data => data.json())
		.then(response => response.response);
}
