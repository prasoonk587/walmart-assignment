import React from 'react';
import Header from './components/Header';
import ConversationPane from './components/ConversationPane';

function App() {
	return (
		<div className="messenger">
			<Header />
			<ConversationPane />
		</div>
	);
}

export default App;
