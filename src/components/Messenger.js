import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import MessageBubble from './MessageBubble';

function Messenger({ messages }) {
	const messagesEndRef = useRef();

	useEffect(() => {
		messagesEndRef ? messagesEndRef.current.lastChild.scrollIntoViewIfNeeded() : null;
	}, []);
	const renderMessages = () => {
		return messages.map(message => {
			return <MessageBubble key={message.id} messageObj={message} />;
		});
	};
	return <div ref={messagesEndRef}>{renderMessages()}</div>;
}
Messenger.propTypes = {
	message: PropTypes.array,
};

export default Messenger;
