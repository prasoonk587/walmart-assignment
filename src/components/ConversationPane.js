import React, { useEffect, useState } from 'react';
import { serializeMessage } from '../utils/seriallizeMessage';
import { get } from '../utils/http-utils';
import Messenger from './Messenger';
import Loader from './Loader';

const API_ENDPOINT = 'http://localhost:3001/api';

function ConversationPane() {
	const [messages, setMessages] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		getMessages();
	}, []);

	const getMessages = async () => {
		try {
			const [messages, members] = await Promise.all([get(API_ENDPOINT + '/messages'), get(API_ENDPOINT + '/members')]);

			const formattedMessage = serializeMessage(messages, members);
			setMessages(formattedMessage);
			setIsLoading(false);
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<div className="conversationPane">
			{isLoading && <Loader />}
			{!isLoading && <Messenger messages={messages} />}
		</div>
	);
}
export default ConversationPane;
