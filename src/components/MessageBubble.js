import React from 'react';
import PropTypes from 'prop-types';
import profileImage from '../../assets/images/profile.jpeg';

function MessageBubble({ messageObj }) {
	const { userName, email, avatar, message, timestamp } = messageObj;
	return (
		<div className="message-bubble">
			<div className="avatar-wrap">
				<div className="avatar" style={{ backgroundImage: `url(${avatar ? avatar : profileImage})` }}></div>
				<div className="tooltip">
					<div>
						<p>{email}</p>
					</div>
				</div>
			</div>
			<div className="message-container">
				<div>
					<p>{userName}</p>
				</div>
				<div className="message-wrap">
					<div>
						<p>{message}</p>
					</div>
				</div>
				<div className="timestamp-wrap">
					<div>
						<p>{timestamp}</p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default React.memo(MessageBubble);

MessageBubble.propTypes = {
	messageObj: PropTypes.object,
};
